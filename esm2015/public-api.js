/*
 * Public API Surface of ngx-matomo
 */
export * from './lib/matomo-injector.service';
export * from './lib/matomo-tracker.service';
export * from './lib/matomo.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3Byb2plY3RzL25neC1tYXRvbW8vc3JjL3B1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0dBRUc7QUFFSCxjQUFjLCtCQUErQixDQUFDO0FBQzlDLGNBQWMsOEJBQThCLENBQUM7QUFDN0MsY0FBYyxxQkFBcUIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2Ygbmd4LW1hdG9tb1xuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL21hdG9tby1pbmplY3Rvci5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21hdG9tby10cmFja2VyLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbWF0b21vLm1vZHVsZSc7XG4iXX0=