import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
/**
 * Service for injecting the Matomo tracker in the application.
 *
 * @export
 */
export class MatomoInjector {
    /**
     * Creates an instance of MatomoInjector.
     *
     * @param platformId Angular description of the platform.
     */
    constructor(platformId) {
        this.platformId = platformId;
        if (isPlatformBrowser(this.platformId)) {
            window._paq = window._paq || [];
        }
        else {
            console.warn('MatomoInjector can\'t be used on server platform');
        }
    }
    /**
     * Injects the Matomo tracker in the DOM.
     *
     * @param url URL of the Matomo instance to connect to.
     * @param id SiteId for this application/site.
     * @param [skipTrackingInitialPageView] Optional boolean which, if true, will prevent tracking the initial page as part of init
     * @param [scriptUrl] Optional URL for the `piwik.js`/`matomo.js` script in case it is not at its default location.
     */
    init(url, id, skipTrackingInitialPageView, scriptUrl) {
        if (isPlatformBrowser(this.platformId)) {
            if (!skipTrackingInitialPageView) {
                window._paq.push(['trackPageView']);
            }
            window._paq.push(['enableLinkTracking']);
            (() => {
                const u = url;
                window._paq.push(['setTrackerUrl', u + 'piwik.php']);
                window._paq.push(['setSiteId', id.toString()]);
                const d = document;
                const g = d.createElement('script');
                const s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = !!scriptUrl ? scriptUrl : u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        }
    }
}
MatomoInjector.decorators = [
    { type: Injectable }
];
MatomoInjector.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [PLATFORM_ID,] }] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0b21vLWluamVjdG9yLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtbWF0b21vL3NyYy9saWIvbWF0b21vLWluamVjdG9yLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBV3BEOzs7O0dBSUc7QUFFSCxNQUFNLE9BQU8sY0FBYztJQUN6Qjs7OztPQUlHO0lBQ0gsWUFBeUMsVUFBVTtRQUFWLGVBQVUsR0FBVixVQUFVLENBQUE7UUFDakQsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDdEMsTUFBTSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztTQUNqQzthQUFNO1lBQ0wsT0FBTyxDQUFDLElBQUksQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO1NBQ2xFO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSCxJQUFJLENBQUMsR0FBVyxFQUFFLEVBQVUsRUFBRSwyQkFBcUMsRUFBRSxTQUFrQjtRQUNyRixJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUN0QyxJQUFJLENBQUMsMkJBQTJCLEVBQUU7Z0JBQ2hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQzthQUNyQztZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLENBQUMsR0FBRyxFQUFFO2dCQUNKLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQztnQkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDckQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDL0MsTUFBTSxDQUFDLEdBQUcsUUFBUSxDQUFDO2dCQUNuQixNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNwQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLENBQUMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7Z0JBQzNCLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUNmLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUNmLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDO2dCQUNqRCxDQUFDLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUNOO0lBQ0gsQ0FBQzs7O1lBM0NGLFVBQVU7Ozs0Q0FPSSxNQUFNLFNBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgUExBVEZPUk1fSUQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGlzUGxhdGZvcm1Ccm93c2VyIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuLyoqXG4gKiBBY2Nlc3MgdG8gdGhlIGdsb2JhbCB3aW5kb3cgdmFyaWFibGUuXG4gKi9cbmRlY2xhcmUgdmFyIHdpbmRvdzoge1xuICBba2V5OiBzdHJpbmddOiBhbnk7XG4gIHByb3RvdHlwZTogV2luZG93O1xuICBuZXcgKCk6IFdpbmRvdztcbn07XG5cbi8qKlxuICogU2VydmljZSBmb3IgaW5qZWN0aW5nIHRoZSBNYXRvbW8gdHJhY2tlciBpbiB0aGUgYXBwbGljYXRpb24uXG4gKlxuICogQGV4cG9ydFxuICovXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTWF0b21vSW5qZWN0b3Ige1xuICAvKipcbiAgICogQ3JlYXRlcyBhbiBpbnN0YW5jZSBvZiBNYXRvbW9JbmplY3Rvci5cbiAgICpcbiAgICogQHBhcmFtIHBsYXRmb3JtSWQgQW5ndWxhciBkZXNjcmlwdGlvbiBvZiB0aGUgcGxhdGZvcm0uXG4gICAqL1xuICBjb25zdHJ1Y3RvcihASW5qZWN0KFBMQVRGT1JNX0lEKSBwcml2YXRlIHBsYXRmb3JtSWQpIHtcbiAgICBpZiAoaXNQbGF0Zm9ybUJyb3dzZXIodGhpcy5wbGF0Zm9ybUlkKSkge1xuICAgICAgd2luZG93Ll9wYXEgPSB3aW5kb3cuX3BhcSB8fCBbXTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc29sZS53YXJuKCdNYXRvbW9JbmplY3RvciBjYW5cXCd0IGJlIHVzZWQgb24gc2VydmVyIHBsYXRmb3JtJyk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEluamVjdHMgdGhlIE1hdG9tbyB0cmFja2VyIGluIHRoZSBET00uXG4gICAqXG4gICAqIEBwYXJhbSB1cmwgVVJMIG9mIHRoZSBNYXRvbW8gaW5zdGFuY2UgdG8gY29ubmVjdCB0by5cbiAgICogQHBhcmFtIGlkIFNpdGVJZCBmb3IgdGhpcyBhcHBsaWNhdGlvbi9zaXRlLlxuICAgKiBAcGFyYW0gW3NraXBUcmFja2luZ0luaXRpYWxQYWdlVmlld10gT3B0aW9uYWwgYm9vbGVhbiB3aGljaCwgaWYgdHJ1ZSwgd2lsbCBwcmV2ZW50IHRyYWNraW5nIHRoZSBpbml0aWFsIHBhZ2UgYXMgcGFydCBvZiBpbml0XG4gICAqIEBwYXJhbSBbc2NyaXB0VXJsXSBPcHRpb25hbCBVUkwgZm9yIHRoZSBgcGl3aWsuanNgL2BtYXRvbW8uanNgIHNjcmlwdCBpbiBjYXNlIGl0IGlzIG5vdCBhdCBpdHMgZGVmYXVsdCBsb2NhdGlvbi5cbiAgICovXG4gIGluaXQodXJsOiBzdHJpbmcsIGlkOiBudW1iZXIsIHNraXBUcmFja2luZ0luaXRpYWxQYWdlVmlldz86IGJvb2xlYW4sIHNjcmlwdFVybD86IHN0cmluZykge1xuICAgIGlmIChpc1BsYXRmb3JtQnJvd3Nlcih0aGlzLnBsYXRmb3JtSWQpKSB7XG4gICAgICBpZiAoIXNraXBUcmFja2luZ0luaXRpYWxQYWdlVmlldykge1xuICAgICAgICB3aW5kb3cuX3BhcS5wdXNoKFsndHJhY2tQYWdlVmlldyddKTtcbiAgICAgIH1cbiAgICAgIHdpbmRvdy5fcGFxLnB1c2goWydlbmFibGVMaW5rVHJhY2tpbmcnXSk7XG4gICAgICAoKCkgPT4ge1xuICAgICAgICBjb25zdCB1ID0gdXJsO1xuICAgICAgICB3aW5kb3cuX3BhcS5wdXNoKFsnc2V0VHJhY2tlclVybCcsIHUgKyAncGl3aWsucGhwJ10pO1xuICAgICAgICB3aW5kb3cuX3BhcS5wdXNoKFsnc2V0U2l0ZUlkJywgaWQudG9TdHJpbmcoKV0pO1xuICAgICAgICBjb25zdCBkID0gZG9jdW1lbnQ7XG4gICAgICAgIGNvbnN0IGcgPSBkLmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgICBjb25zdCBzID0gZC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0JylbMF07XG4gICAgICAgIGcudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnO1xuICAgICAgICBnLmFzeW5jID0gdHJ1ZTtcbiAgICAgICAgZy5kZWZlciA9IHRydWU7XG4gICAgICAgIGcuc3JjID0gISFzY3JpcHRVcmwgPyBzY3JpcHRVcmwgOiB1ICsgJ3Bpd2lrLmpzJztcbiAgICAgICAgcy5wYXJlbnROb2RlLmluc2VydEJlZm9yZShnLCBzKTtcbiAgICAgIH0pKCk7XG4gICAgfVxuICB9XG59XG4iXX0=