(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('ngx-matomo-v9', ['exports', '@angular/core', '@angular/common'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['ngx-matomo-v9'] = {}, global.ng.core, global.ng.common));
}(this, (function (exports, core, common) { 'use strict';

    /**
     * Service for injecting the Matomo tracker in the application.
     *
     * @export
     */
    var MatomoInjector = /** @class */ (function () {
        /**
         * Creates an instance of MatomoInjector.
         *
         * @param platformId Angular description of the platform.
         */
        function MatomoInjector(platformId) {
            this.platformId = platformId;
            if (common.isPlatformBrowser(this.platformId)) {
                window._paq = window._paq || [];
            }
            else {
                console.warn('MatomoInjector can\'t be used on server platform');
            }
        }
        /**
         * Injects the Matomo tracker in the DOM.
         *
         * @param url URL of the Matomo instance to connect to.
         * @param id SiteId for this application/site.
         * @param [skipTrackingInitialPageView] Optional boolean which, if true, will prevent tracking the initial page as part of init
         * @param [scriptUrl] Optional URL for the `piwik.js`/`matomo.js` script in case it is not at its default location.
         */
        MatomoInjector.prototype.init = function (url, id, skipTrackingInitialPageView, scriptUrl) {
            if (common.isPlatformBrowser(this.platformId)) {
                if (!skipTrackingInitialPageView) {
                    window._paq.push(['trackPageView']);
                }
                window._paq.push(['enableLinkTracking']);
                (function () {
                    var u = url;
                    window._paq.push(['setTrackerUrl', u + 'piwik.php']);
                    window._paq.push(['setSiteId', id.toString()]);
                    var d = document;
                    var g = d.createElement('script');
                    var s = d.getElementsByTagName('script')[0];
                    g.type = 'text/javascript';
                    g.async = true;
                    g.defer = true;
                    g.src = !!scriptUrl ? scriptUrl : u + 'piwik.js';
                    s.parentNode.insertBefore(g, s);
                })();
            }
        };
        return MatomoInjector;
    }());
    MatomoInjector.decorators = [
        { type: core.Injectable }
    ];
    MatomoInjector.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: core.Inject, args: [core.PLATFORM_ID,] }] }
    ]; };

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __rest(s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); };
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    var __createBinding = Object.create ? (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function () { return m[k]; } });
    }) : (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        o[k2] = m[k];
    });
    function __exportStar(m, exports) {
        for (var p in m)
            if (p !== "default" && !exports.hasOwnProperty(p))
                __createBinding(exports, m, p);
    }
    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m)
            return m.call(o);
        if (o && typeof o.length === "number")
            return {
                next: function () {
                    if (o && i >= o.length)
                        o = void 0;
                    return { value: o && o[i++], done: !o };
                }
            };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    ;
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }
    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n])
            i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try {
            step(g[n](v));
        }
        catch (e) {
            settle(q[0][3], e);
        } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length)
            resume(q[0][0], q[0][1]); }
    }
    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }
    function __asyncValues(o) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
    }
    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
            Object.defineProperty(cooked, "raw", { value: raw });
        }
        else {
            cooked.raw = raw;
        }
        return cooked;
    }
    ;
    var __setModuleDefault = Object.create ? (function (o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function (o, v) {
        o["default"] = v;
    };
    function __importStar(mod) {
        if (mod && mod.__esModule)
            return mod;
        var result = {};
        if (mod != null)
            for (var k in mod)
                if (Object.hasOwnProperty.call(mod, k))
                    __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }
    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }
    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }
    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    /**
     * Wrapper for functions available in the Matomo Javascript tracker.
     *
     * @export
     */
    var MatomoTracker = /** @class */ (function () {
        /**
         * Creates an instance of MatomoTracker.
         */
        function MatomoTracker() {
            try {
                if (typeof window._paq === 'undefined') {
                    console.warn('Matomo has not yet been initialized! (Did you forget to inject it?)');
                }
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        }
        /**
         * Logs a visit to this page.
         *
         * @param [customTitle] Optional title of the visited page.
         */
        MatomoTracker.prototype.trackPageView = function (customTitle) {
            try {
                var args = [];
                if (!!customTitle) {
                    args.push(customTitle);
                }
                window._paq.push(__spread(['trackPageView'], args));
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Logs an event with an event category (Videos, Music, Games…), an event action (Play, Pause, Duration,
         * Add Playlist, Downloaded, Clicked…), and an optional event name and optional numeric value.
         *
         * @param category Category of the event.
         * @param action Action of the event.
         * @param [name] Optional name of the event.
         * @param [value] Optional value for the event.
         */
        MatomoTracker.prototype.trackEvent = function (category, action, name, value) {
            try {
                var args = [category, action];
                if (!!name) {
                    args.push(name);
                }
                if (typeof value === 'number') {
                    args.push(value);
                }
                window._paq.push(__spread(['trackEvent'], args));
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Logs an internal site search for a specific keyword, in an optional category,
         * specifying the optional count of search results in the page.
         *
         * @param keyword Keywords of the search query.
         * @param [category] Optional category of the search query.
         * @param [resultsCount] Optional number of results returned by the search query.
         */
        MatomoTracker.prototype.trackSiteSearch = function (keyword, category, resultsCount) {
            try {
                var args = [keyword];
                if (!!category) {
                    args.push(category);
                }
                if (typeof resultsCount === 'number') {
                    args.push(resultsCount);
                }
                window._paq.push(__spread(['trackSiteSearch'], args));
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Manually logs a conversion for the numeric goal ID, with an optional numeric custom revenue customRevenue.
         *
         * @param idGoal numeric ID of the goal to log a conversion for.
         * @param [customRevenue] Optional custom revenue to log for the goal.
         */
        MatomoTracker.prototype.trackGoal = function (idGoal, customRevenue) {
            try {
                var args = [idGoal];
                if (typeof customRevenue === 'number') {
                    args.push(customRevenue);
                }
                window._paq.push(__spread(['trackGoal'], args));
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Manually logs a click from your own code.
         *
         * @param url Full URL which is to be tracked as a click.
         * @param linkType Either 'link' for an outlink or 'download' for a download.
         */
        MatomoTracker.prototype.trackLink = function (url, linkType) {
            try {
                window._paq.push(['trackLink', url, linkType]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Scans the entire DOM for all content blocks and tracks all impressions once the DOM ready event has been triggered.
         *
         * @see {@link https://developer.matomo.org/guides/content-tracking|Content Tracking}
         */
        MatomoTracker.prototype.trackAllContentImpressions = function () {
            try {
                window._paq.push(['trackAllContentImpressions']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Scans the entire DOM for all content blocks as soon as the page is loaded.<br />
         * It tracks an impression only if a content block is actually visible.
         *
         * @param checkOnScroll If true, checks for new content blocks while scrolling the page.
         * @param timeInterval Duration, in milliseconds, between two checks upon scroll.
         * @see {@link https://developer.matomo.org/guides/content-tracking|Content Tracking}
         */
        MatomoTracker.prototype.trackVisibleContentImpressions = function (checkOnScroll, timeInterval) {
            try {
                window._paq.push(['trackVisibleContentImpressions', checkOnScroll, timeInterval]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Scans the given DOM node and its children for content blocks and tracks an impression for them
         * if no impression was already tracked for it.
         *
         * @param node DOM node in which to look for content blocks which have not been previously tracked.
         * @see {@link https://developer.matomo.org/guides/content-tracking|Content Tracking}
         */
        MatomoTracker.prototype.trackContentImpressionsWithinNode = function (node) {
            try {
                window._paq.push(['trackContentImpressionsWithinNode', node]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Tracks an interaction with the given DOM node/content block.
         *
         * @param node DOM node for which to track a content interaction.
         * @param contentInteraction Name of the content interaction.
         * @see {@link https://developer.matomo.org/guides/content-tracking|Content Tracking}
         */
        MatomoTracker.prototype.trackContentInteractionNode = function (node, contentInteraction) {
            try {
                window._paq.push(['trackContentInteractionNode', node, contentInteraction]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Tracks a content impression using the specified values.
         *
         * @param contentName Content name.
         * @param contentPiece Content piece.
         * @param contentTarget Content target.
         * @see {@link https://developer.matomo.org/guides/content-tracking|Content Tracking}
         */
        MatomoTracker.prototype.trackContentImpression = function (contentName, contentPiece, contentTarget) {
            try {
                window._paq.push(['trackContentImpression', contentName, contentPiece, contentTarget]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Tracks a content interaction using the specified values.
         *
         * @param contentInteraction Content interaction.
         * @param contentName Content name.
         * @param contentPiece Content piece.
         * @param contentTarget Content target.
         * @see {@link https://developer.matomo.org/guides/content-tracking|Content Tracking}
         */
        MatomoTracker.prototype.trackContentInteraction = function (contentInteraction, contentName, contentPiece, contentTarget) {
            try {
                window._paq.push([
                    'trackContentInteraction',
                    contentInteraction,
                    contentName,
                    contentPiece,
                    contentTarget
                ]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Logs all found content blocks within a page to the console. This is useful to debug / test content tracking.
         */
        MatomoTracker.prototype.logAllContentBlocksOnPage = function () {
            try {
                window._paq.push(['logAllContentBlocksOnPage']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Install a Heart beat timer that will regularly send requests to Matomo in order to better measure the time spent on the page.<br />
         * These requests will be sent only when the user is actively viewing the page (when the tab is active and in focus).<br />
         * These requests will not track additional actions or page views.<br />
         * By default, the delay is set to 15 seconds.
         *
         * @param delay Delay, in seconds, between two heart beats to the server.
         */
        MatomoTracker.prototype.enableHeartBeatTimer = function (delay) {
            try {
                window._paq.push(['enableHeartBeatTimer', delay]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Installs link tracking on all applicable link elements.
         *
         * @param enable Set the enable parameter to true to use pseudo click-handler (treat middle click and open contextmenu as
         * left click).<br />
         * A right click (or any click that opens the context menu) on a link will be tracked as clicked even if "Open in new tab"
         * is not selected.<br />
         * If "false" (default), nothing will be tracked on open context menu or middle click.
         */
        MatomoTracker.prototype.enableLinkTracking = function (enable) {
            try {
                window._paq.push(['enableLinkTracking', enable]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Enables cross domain linking. By default, the visitor ID that identifies a unique visitor is stored in the browser's
         * first party cookies.<br />
         * This means the cookie can only be accessed by pages on the same domain.<br />
         * If you own multiple domains and would like to track all the actions and pageviews of a specific visitor into the same visit,
         * you may enable cross domain linking.<br />
         * Whenever a user clicks on a link it will append a URL parameter pk_vid to the clicked URL which forwards the current
         * visitor ID value to the page of the different domain.
         *
         * @see {@link https://matomo.org/faq/how-to/faq_23654/|Cross Domain Linking}
         */
        MatomoTracker.prototype.enableCrossDomainLinking = function () {
            try {
                window._paq.push(['enableCrossDomainLinking']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * By default, the two visits across domains will be linked together when the link is clicked and the page is loaded within
         * a 180 seconds timeout window.
         *
         * @param timeout Timeout, in seconds, between two actions across two domanes before creating a new visit.
         * @see {@link https://matomo.org/faq/how-to/faq_23654/|Cross Domain Linking}
         */
        MatomoTracker.prototype.setCrossDomainLinkingTimeout = function (timeout) {
            try {
                window._paq.push(['setCrossDomainLinkingTimeout', timeout]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Overrides document.title
         *
         * @param title Title of the document.
         */
        MatomoTracker.prototype.setDocumentTitle = function (title) {
            try {
                window._paq.push(['setDocumentTitle', title]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets array of hostnames or domains to be treated as local.<br />
         * For wildcard subdomains, you can use: `setDomains('.example.com')`; or `setDomains('*.example.com');`.<br />
         * You can also specify a path along a domain: `setDomains('*.example.com/subsite1');`.
         *
         * @param domains List of hostnames or domains, with or without path, to be treated as local.
         * @see {@link https://matomo.org/faq/how-to/faq_23654/|Cross Domain Linking}
         */
        MatomoTracker.prototype.setDomains = function (domains) {
            try {
                window._paq.push(['setDomains', domains]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Override the page's reported URL.
         *
         * @param url URL to be reported for the page.
         */
        MatomoTracker.prototype.setCustomUrl = function (url) {
            try {
                window._paq.push(['setCustomUrl', url]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Overrides the detected Http-Referer.
         *
         * @param url URL to be reported for the referer.
         */
        MatomoTracker.prototype.setReferrerUrl = function (url) {
            try {
                window._paq.push(['setReferrerUrl', url]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Specifies the website ID.<br />
         * Redundant: can be specified in getTracker() constructor.
         *
         * @param siteId Site ID for the tracker.
         */
        MatomoTracker.prototype.setSiteId = function (siteId) {
            try {
                window._paq.push(['setSiteId', siteId]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Specify the Matomo HTTP API URL endpoint. Points to the root directory of matomo,
         * e.g. http://matomo.example.org/ or https://example.org/matomo/.<br />
         * This function is only useful when the 'Overlay' report is not working.<br />
         * By default, you do not need to use this function.
         *
         * @param url URL for Matomo HTTP API endpoint.
         */
        MatomoTracker.prototype.setApiUrl = function (url) {
            try {
                window._paq.push(['setApiUrl', url]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Specifies the Matomo server URL.<br />
         * Redundant: can be specified in getTracker() constructor.
         *
         * @param url URL for the Matomo server.
         */
        MatomoTracker.prototype.setTrackerUrl = function (url) {
            try {
                window._paq.push(['setTrackerUrl', url]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Returns the Matomo server URL.
         *
         * @returns Promise for the Matomo server URL.
         */
        MatomoTracker.prototype.getPiwikUrl = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getPiwikUrl());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the current url of the page that is currently being visited.<br />
         * If a custom URL was set before calling this method, the custom URL will be returned.
         *
         * @returns Promise for the URL of the current page.
         */
        MatomoTracker.prototype.getCurrentUrl = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getCurrentUrl());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Sets classes to be treated as downloads (in addition to piwik_download).
         *
         * @param classes Class, or list of classes to be treated as downloads.
         */
        MatomoTracker.prototype.setDownloadClasses = function (classes) {
            try {
                window._paq.push(['setDownloadClasses', classes]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets list of file extensions to be recognized as downloads.<br />
         * Example: `'docx'` or `['docx', 'xlsx']`.
         *
         * @param extensions Extension, or list of extensions to be recognized as downloads.
         */
        MatomoTracker.prototype.setDownloadExtensions = function (extensions) {
            try {
                window._paq.push(['setDownloadClasses', extensions]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets additional file extensions to be recognized as downloads.<br />
         * Example: `'docx'` or `['docx', 'xlsx']`.
         *
         * @param extensions Extension, or list of extensions to be recognized as downloads.
         */
        MatomoTracker.prototype.addDownloadExtensions = function (extensions) {
            try {
                window._paq.push(['setDownloadClasses', extensions]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets file extensions to be removed from the list of download file extensions.<br />
         * Example: `'docx'` or `['docx', 'xlsx']`.
         *
         * @param extensions Extension, or list of extensions not to be recognized as downloads.
         */
        MatomoTracker.prototype.removeDownloadExtensions = function (extensions) {
            try {
                window._paq.push(['setDownloadClasses', extensions]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets classes to be ignored if present in link (in addition to piwik_ignore).
         *
         * @param classes Class, or list of classes to be ignored if present in link.
         */
        MatomoTracker.prototype.setIgnoreClasses = function (classes) {
            try {
                window._paq.push(['setDownloadClasses', classes]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Set classes to be treated as outlinks (in addition to piwik_link).
         *
         * @param classes Class, or list of classes to be treated as outlinks.
         */
        MatomoTracker.prototype.setLinkClasses = function (classes) {
            try {
                window._paq.push(['setDownloadClasses', classes]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Set delay for link tracking (in milliseconds).
         *
         * @param delay Delay, in milliseconds, for link tracking.
         */
        MatomoTracker.prototype.setLinkTrackingTimer = function (delay) {
            try {
                window._paq.push(['setLinkTrackingTimer', delay]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Returns delay for link tracking.
         *
         * @returns Promise for the delay in milliseconds.
         */
        MatomoTracker.prototype.getLinkTrackingTimer = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getLinkTrackingTimer());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Set to true to not record the hash tag (anchor) portion of URLs.
         *
         * @param value If true, the hash tag portion of the URLs won't be recorded.
         */
        MatomoTracker.prototype.discardHashTag = function (value) {
            try {
                window._paq.push(['discardHashTag', value]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * By default Matomo uses the browser DOM Timing API to accurately determine the time it takes to generate and download
         * the page. You may overwrite this value with this function.
         *
         * @param generationTime Time, in milliseconds, of the page generation.
         */
        MatomoTracker.prototype.setGenerationTimeMs = function (generationTime) {
            try {
                window._paq.push(['setGenerationTimeMs', generationTime]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Appends a custom string to the end of the HTTP request to piwik.php.
         *
         * @param appendToUrl String to append to the end of the HTTP request to piwik.php/matomo.php.
         */
        MatomoTracker.prototype.appendToTrackingUrl = function (appendToUrl) {
            try {
                window._paq.push(['appendToTrackingUrl', appendToUrl]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Set to true to not track users who opt out of tracking using Mozilla's (proposed) Do Not Track setting.
         *
         * @param doNotTrack If true, users who opted for Do Not Track in their settings won't be tracked.
         * @see {@link https://www.w3.org/TR/tracking-dnt/}
         */
        MatomoTracker.prototype.setDoNotTrack = function (doNotTrack) {
            try {
                window._paq.push(['setDoNotTrack', doNotTrack]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Enables a frame-buster to prevent the tracked web page from being framed/iframed.
         */
        MatomoTracker.prototype.killFrame = function () {
            try {
                window._paq.push(['killFrame']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Forces the browser to load the live URL if the tracked web page is loaded from a local file
         * (e.g., saved to someone's desktop).
         *
         * @param url URL to track instead of file:// URLs.
         */
        MatomoTracker.prototype.redirectFile = function (url) {
            try {
                window._paq.push(['redirectFile', url]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Records how long the page has been viewed if the minimumVisitLength is attained;
         * the heartBeatDelay determines how frequently to update the server.
         *
         * @param minimumVisitLength Duration before notifying the server for the duration of the visit to a page.
         * @param heartBeatDelay Delay, in seconds, between two updates to the server.
         */
        MatomoTracker.prototype.setHeartBeatTimer = function (minimumVisitLength, heartBeatDelay) {
            try {
                window._paq.push(['setHeartBeatTimer', minimumVisitLength, heartBeatDelay]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Returns the 16 characters ID for the visitor.
         *
         * @returns Promise for the the 16 characters ID for the visitor.
         */
        MatomoTracker.prototype.getVisitorId = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getVisitorId());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the visitor cookie contents in an array.
         *
         * @returns Promise for the cookie contents in an array.
         */
        MatomoTracker.prototype.getVisitorInfo = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getVisitorInfo());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the visitor attribution array (Referer information and/or Campaign name & keyword).<br />
         * Attribution information is used by Matomo to credit the correct referrer (first or last referrer)
         * used when a user triggers a goal conversion.
         *
         * @returns Promise for the visitor attribution array (Referer information and/or Campaign name & keyword).
         */
        MatomoTracker.prototype.getAttributionInfo = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getAttributionInfo());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the attribution campaign name.
         *
         * @returns Promise for the the attribution campaign name.
         */
        MatomoTracker.prototype.getAttributionCampaignName = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getAttributionCampaignName());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the attribution campaign keyword.
         *
         * @returns Promise for the attribution campaign keyword.
         */
        MatomoTracker.prototype.getAttributionCampaignKeyword = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getAttributionCampaignKeyword());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the attribution referrer timestamp.
         *
         * @returns Promise for the attribution referrer timestamp (as string).
         */
        MatomoTracker.prototype.getAttributionReferrerTimestamp = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getAttributionReferrerTimestamp());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the attribution referrer URL.
         *
         * @returns Promise for the attribution referrer URL
         */
        MatomoTracker.prototype.getAttributionReferrerUrl = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getAttributionReferrerUrl());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Returns the User ID string if it was set.
         *
         * @returns Promise for the User ID for the visitor.
         * @see {@link https://matomo.org/docs/user-id/|Matomo User ID}
         */
        MatomoTracker.prototype.getUserId = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getUserId());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Sets a User ID to this user (such as an email address or a username).
         *
         * @param userId User ID to set for the current visitor.
         * @see {@link https://matomo.org/docs/user-id/|Matomo User ID}
         */
        MatomoTracker.prototype.setUserId = function (userId) {
            try {
                window._paq.push(['setUserId', userId]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Resets the User ID which also generates a new Visitor ID.
         *
         * @see {@link https://matomo.org/docs/user-id/|Matomo User ID}
         */
        MatomoTracker.prototype.resetUserId = function () {
            try {
                window._paq.push(['resetUserId']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets a custom variable.
         *
         * @param index Index, the number from 1 to 5 where this custom variable name is stored for the current page view.
         * @param name Name, the name of the variable, for example: Category, Sub-category, UserType.
         * @param value Value, for example: "Sports", "News", "World", "Business"…
         * @param scope Scope of the custom variable:<br />
         * - "page" means the custom variable applies to the current page view.
         * - "visit" means the custom variable applies to the current visitor.
         * @see {@link https://matomo.org/docs/custom-variables/|Custom Variables}
         */
        MatomoTracker.prototype.setCustomVariable = function (index, name, value, scope) {
            try {
                window._paq.push(['setCustomVariable', index, name, value, scope]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Deletes a custom variable.
         *
         * @param index Index of the custom variable to delete.
         * @param scope Scope of the custom variable to delete.
         * @see {@link https://matomo.org/docs/custom-variables/|Custom Variables}
         */
        MatomoTracker.prototype.deleteCustomVariable = function (index, scope) {
            try {
                window._paq.push(['deleteCustomVariable', index, scope]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Deletes all custom variables.
         *
         * @param scope Scope of the custom variables to delete.
         * @see {@link https://matomo.org/docs/custom-variables/|Custom Variables}
         */
        MatomoTracker.prototype.deleteCustomVariables = function (scope) {
            try {
                window._paq.push(['deleteCustomVariables', scope]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Retrieves a custom variable.
         *
         * @param index Index of the custom variable to retrieve.
         * @param scope Scope of the custom variable to retrieve.
         * @returns Promise for the value of custom variable.
         * @see {@link https://matomo.org/docs/custom-variables/|Custom Variables}
         */
        MatomoTracker.prototype.getCustomVariable = function (index, scope) {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getCustomVariable(index, scope));
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * When called then the Custom Variables of scope "visit" will be stored (persisted) in a first party cookie
         * for the duration of the visit.<br />
         * This is useful if you want to call getCustomVariable later in the visit.<br />
         * (by default custom variables are not stored on the visitor's computer.)
         *
         * @see {@link https://matomo.org/docs/custom-variables/|Custom Variables}
         */
        MatomoTracker.prototype.storeCustomVariablesInCookie = function () {
            try {
                window._paq.push(['storeCustomVariablesInCookie']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets a custom dimension.<br />
         * (requires Matomo 2.15.1 + Custom Dimensions plugin)
         *
         * @param customDimensionId ID of the custom dimension to set.
         * @param customDimensionValue Value to be set.
         * @see {@link https://plugins.piwik.org/CustomDimensions|Custom Dimensions}
         */
        MatomoTracker.prototype.setCustomDimension = function (customDimensionId, customDimensionValue) {
            try {
                window._paq.push(['setCustomDimension', customDimensionId, customDimensionValue]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Deletes a custom dimension.<br />
         * (requires Matomo 2.15.1 + Custom Dimensions plugin)
         *
         * @param customDimensionId ID of the custom dimension to delete.
         * @see {@link https://plugins.piwik.org/CustomDimensions|Custom Dimensions}
         */
        MatomoTracker.prototype.deleteCustomDimension = function (customDimensionId) {
            try {
                window._paq.push(['deleteCustomDimension', customDimensionId]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Retrieve a custom dimension.<br />
         * (requires Matomo 2.15.1 + Custom Dimensions plugin)
         *
         * @param customDimensionId ID of the custom dimension to retrieve.
         * @returns Promise for the value for the requested custom dimension.
         * @see {@link https://plugins.piwik.org/CustomDimensions|Custom Dimensions}
         */
        MatomoTracker.prototype.getCustomDimension = function (customDimensionId) {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.getCustomDimension(customDimensionId));
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Sets campaign name parameter(s).
         *
         * @param name Name of the campaign
         * @see {@link https://matomo.org/docs/tracking-campaigns/|Campaigns}
         */
        MatomoTracker.prototype.setCampaignNameKey = function (name) {
            try {
                window._paq.push(['setCampaignNameKey', name]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets campaign keyword parameter(s).
         *
         * @param keyword Keyword parameter(s) of the campaign.
         * @see {@link https://matomo.org/docs/tracking-campaigns/|Campaigns}
         */
        MatomoTracker.prototype.setCampaignKeywordKey = function (keyword) {
            try {
                window._paq.push(['setCampaignKeywordKey', keyword]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Set to true to attribute a conversion to the first referrer.<br />
         * By default, conversion is attributed to the most recent referrer.
         *
         * @param conversionToFirstReferrer If true, Matomo will attribute the Goal conversion to the first referrer used
         * instead of the last one.
         * @see {@link https://matomo.org/docs/tracking-campaigns/|Campaigns}
         * @see {@link https://matomo.org/faq/general/faq_106/#faq_106|Conversions to the first referrer}
         */
        MatomoTracker.prototype.setConversionAttributionFirstReferrer = function (conversionToFirstReferrer) {
            try {
                window._paq.push(['setConversionAttributionFirstReferrer', conversionToFirstReferrer]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets the current page view as a product or category page view.<br />
         * When you call setEcommerceView, it must be followed by a call to trackPageView to record the product or category page view.
         *
         * @param productSKU SKU of the viewed product.
         * @param productName Name of the viewed product.
         * @param productCategory Category of the viewed product.
         * @param price Price of the viewed product.
         */
        MatomoTracker.prototype.setEcommerceView = function (productSKU, productName, productCategory, price) {
            try {
                window._paq.push(['setEcommerceView', productSKU, productName, productCategory, price]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Adds a product into the eCommerce order.<br />
         * Must be called for each product in the order.
         *
         * @param productSKU SKU of the product to add.
         * @param [productName] Optional name of the product to add.
         * @param [productCategory] Optional category of the product to add.
         * @param [price] Optional price of the product to add.
         * @param [quantity] Optional quantity of the product to add.
         */
        MatomoTracker.prototype.addEcommerceItem = function (productSKU, productName, productCategory, price, quantity) {
            try {
                var args = [productSKU];
                if (!!productName) {
                    args.push(productName);
                }
                if (!!productCategory) {
                    args.push(productCategory);
                }
                if (typeof price === 'number') {
                    args.push(price);
                }
                if (typeof quantity === 'number') {
                    args.push(quantity);
                }
                window._paq.push(__spread(['addEcommerceItem'], args));
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Tracks a shopping cart.<br />
         * Call this javascript function every time a user is adding, updating or deleting a product from the cart.
         *
         * @param grandTotal Grand total of the shopping cart.
         */
        MatomoTracker.prototype.trackEcommerceCartUpdate = function (grandTotal) {
            try {
                window._paq.push(['trackEcommerceCartUpdate', grandTotal]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Tracks an Ecommerce order, including any eCommerce item previously added to the order.<br />
         * orderId and grandTotal (ie.revenue) are required parameters.
         *
         * @param orderId ID of the tracked order.
         * @param grandTotal Grand total of the tracked order.
         * @param [subTotal] Sub total of the tracked order.
         * @param [tax] Taxes for the tracked order.
         * @param [shipping] Shipping fees for the tracked order.
         * @param [discount] Discount granted for the tracked order.
         */
        MatomoTracker.prototype.trackEcommerceOrder = function (orderId, grandTotal, subTotal, tax, shipping, discount) {
            try {
                var args = [orderId, grandTotal];
                if (typeof subTotal === 'number') {
                    args.push(subTotal);
                }
                if (typeof tax === 'number') {
                    args.push(tax);
                }
                if (typeof shipping === 'number') {
                    args.push(shipping);
                }
                if (typeof discount === 'number') {
                    args.push(discount);
                }
                window._paq.push(__spread(['trackEcommerceOrder'], args));
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Disables all first party cookies.<br />
         * Existing Matomo cookies for this websites will be deleted on the next page view.
         */
        MatomoTracker.prototype.disableCookies = function () {
            try {
                window._paq.push(['disableCookies']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Deletes the tracking cookies currently set (useful when creating new visits).
         */
        MatomoTracker.prototype.deleteCookies = function () {
            try {
                window._paq.push(['deleteCookies']);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Returns whether cookies are enabled and supported by this browser.
         *
         * @returns Promise for the support and activation of cookies.
         */
        MatomoTracker.prototype.hasCookies = function () {
            return new Promise(function (resolve, reject) {
                try {
                    window._paq.push([
                        function () {
                            resolve(this.hasCookies());
                        }
                    ]);
                }
                catch (e) {
                    if (!(e instanceof ReferenceError)) {
                        reject(e);
                    }
                }
            });
        };
        /**
         * Sets the tracking cookie name prefix.<br />
         * Default prefix is 'pk'.
         *
         * @param prefix Prefix for the tracking cookie names.
         */
        MatomoTracker.prototype.setCookieNamePrefix = function (prefix) {
            try {
                window._paq.push(['setCookieNamePrefix', prefix]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets the domain of the tracking cookies.<br />
         * Default is the document domain.<br />
         * If your website can be visited at both www.example.com and example.com, you would use: `'.example.com'` or `'*.example.com'`.
         *
         * @param domain Domain of the tracking cookies.
         */
        MatomoTracker.prototype.setCookieDomain = function (domain) {
            try {
                window._paq.push(['setCookieDomain', domain]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets the path of the tracking cookies.<br />
         * Default is '/'.
         *
         * @param path Path of the tracking cookies.
         */
        MatomoTracker.prototype.setCookiePath = function (path) {
            try {
                window._paq.push(['setCookiePath', path]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Set to true to enable the Secure cookie flag on all first party cookies.<br />
         * This should be used when your website is only available under HTTPS so that all tracking cookies are always sent
         * over secure connection.
         *
         * @param secure If true, the secure cookie flag will be set on all first party cookies.
         */
        MatomoTracker.prototype.setSecureCookie = function (secure) {
            try {
                window._paq.push(['setSecureCookie', secure]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets the visitor cookie timeout.<br />
         * Default is 13 months.
         *
         * @param timeout Timeout, in seconds, for the visitor cookie timeout.
         */
        MatomoTracker.prototype.setVisitorCookieTimeout = function (timeout) {
            try {
                window._paq.push(['setVisitorCookieTimeout', timeout]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets the referral cookie timeout.<br />
         * Default is 6 months.
         *
         * @param timeout Timeout, in seconds, for the referral cookie timeout.
         */
        MatomoTracker.prototype.setReferralCookieTimeout = function (timeout) {
            try {
                window._paq.push(['setReferralCookieTimeout', timeout]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets the session cookie timeout.<br />
         * Default is 30 minutes.
         *
         * @param timeout Timeout, in seconds, for the session cookie timeout.
         */
        MatomoTracker.prototype.setSessionCookieTimeout = function (timeout) {
            try {
                window._paq.push(['setSessionCookieTimeout', timeout]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Adds a click listener to a specific link element.<br />
         * When clicked, Matomo will log the click automatically.
         *
         * @param element Element on which to add a click listener.
         */
        MatomoTracker.prototype.addListener = function (element) {
            try {
                window._paq.push(['addListener', element]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets the request method to either "GET" or "POST". (The default is "GET".)<br />
         * To use the POST request method, either:<br />
         * 1) the Matomo host is the same as the tracked website host (Matomo installed in the same domain as your tracked website), or<br />
         * 2) if Matomo is not installed on the same host as your website, you need to enable CORS (Cross domain requests).
         *
         * @param method HTTP method for sending information to the Matomo server.
         */
        MatomoTracker.prototype.setRequestMethod = function (method) {
            try {
                window._paq.push(['setRequestMethod', method]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets a function that will process the request content.<br />
         * The function will be called once the request (query parameters string) has been prepared, and before the request content is sent.
         *
         * @param callback Function that will process the request content.
         */
        MatomoTracker.prototype.setCustomRequestProcessing = function (callback) {
            try {
                window._paq.push(['setCustomRequestProcessing', callback]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        /**
         * Sets request Content-Type header value.<br />
         * Applicable when "POST" request method is used via setRequestMethod.
         *
         * @param contentType Value for Content-Type HTTP header.
         */
        MatomoTracker.prototype.setRequestContentType = function (contentType) {
            try {
                window._paq.push(['setRequestContentType', contentType]);
            }
            catch (e) {
                if (!(e instanceof ReferenceError)) {
                    throw e;
                }
            }
        };
        return MatomoTracker;
    }());
    MatomoTracker.decorators = [
        { type: core.Injectable }
    ];
    MatomoTracker.ctorParameters = function () { return []; };

    var MatomoModule = /** @class */ (function () {
        function MatomoModule() {
        }
        return MatomoModule;
    }());
    MatomoModule.decorators = [
        { type: core.NgModule, args: [{
                    declarations: [],
                    imports: [],
                    exports: [],
                    providers: [MatomoInjector, MatomoTracker]
                },] }
    ];

    /*
     * Public API Surface of ngx-matomo
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.MatomoInjector = MatomoInjector;
    exports.MatomoModule = MatomoModule;
    exports.MatomoTracker = MatomoTracker;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ngx-matomo-v9.umd.js.map
